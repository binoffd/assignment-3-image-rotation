#ifndef NEW_PROJECT_IMAGE_H
#define NEW_PROJECT_IMAGE_H

#include <malloc.h>
#include <stddef.h>
#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel get_pixel(struct image* img, uint64_t i, uint64_t j);
void set_pixel(struct image* img, struct pixel pix, uint64_t i, uint64_t j);

struct image create_image(uint64_t w, uint64_t h);
void destruct_image(struct image img);
#endif
