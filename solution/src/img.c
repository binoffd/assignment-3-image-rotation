#include "img.h"

struct pixel get_pixel(struct image* img, uint64_t i, uint64_t j) {
    struct pixel pix = img->data[i + (j * img->width)];
    return pix;
}

void set_pixel(struct image* img, struct pixel pix, uint64_t i, uint64_t j) {
    img->data[i + (j * img->width)] = pix;
}

struct image create_image(uint64_t w, uint64_t h) {
    return (struct image) {.height = h, .width = w, .data =
            malloc(w * h * sizeof(struct pixel))};
}

void destruct_image(struct image img) {
    free(img.data);
    img.data = NULL;
}
