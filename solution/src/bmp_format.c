#include "bmp_format.h"

static inline uint8_t get_padding(uint64_t width) {
    uint8_t byte_width = width * sizeof(struct pixel);
    uint8_t padding = (4 - byte_width % 4) % 4;
    return padding;
}

static struct bmp_header get_header(uint64_t width, uint64_t height) {
    uint64_t size = width * height * sizeof(struct pixel);
    return (struct bmp_header) {
            .bfType = 0x4D42,
            .biWidth = width,
            .biHeight = height,
            .bfileSize = size + 54,
            .biSizeImage = size,
            .bOffBits = 54,
            .biSize = 40,
            .biPlanes = 1,
            .biBitCount = 24,

            .bfReserved = 0,
            .biCompression = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header bmp_header;
    
    if (!fread(&bmp_header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_BITS;
    uint8_t padding = get_padding(bmp_header.biWidth);
    if (bmp_header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (bmp_header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    *img = create_image(bmp_header.biWidth, bmp_header.biHeight);
    for (uint64_t i = 0; i < bmp_header.biHeight; i++) {
        fread(img->data + i * bmp_header.biWidth, bmp_header.biWidth * sizeof(struct pixel), 1, in);
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header bmp_header = get_header(img->width, img->height);
    if (fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    uint8_t padding = get_padding(img->width);

    for (uint64_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
