#include "rotator.h"
#include "bmp_format.h"
#include <err.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3) err(EXIT_FAILURE, "Неверные аргументы, должно быть 2 файла");

    struct image input_img;
    struct image output_img;
    FILE* input_file = NULL;
    FILE* output_file = NULL;

    input_file = fopen(argv[1], "rb");
    if (input_file == NULL) err(EXIT_FAILURE, "Ошибка чтения входного файла");
    output_file = fopen(argv[2], "wb");
    if (output_file == NULL) err(EXIT_FAILURE, "Ошибка записи в выходной файл");
    from_bmp(input_file, &input_img);
    output_img = rotate(input_img);
    to_bmp(output_file, &output_img);
    if (fclose(output_file)) err(EXIT_FAILURE, "Ошибка при закрытии выходного файла");
    if (fclose(input_file)) err(EXIT_FAILURE, "Ошибка при закрытии входного файла");
    destruct_image(input_img);
    destruct_image(output_img);
    return 0;
}
