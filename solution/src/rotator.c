#include "rotator.h"

struct image rotate(struct image img) {
    struct image new_img = create_image(img.height, img.width);
    for (uint64_t i = 0; i < img.height; i++) {
        for (uint64_t j = 0; j < img.width; j++) {
            struct pixel pix = get_pixel(&img, j, i);
            set_pixel(&new_img, pix, new_img.width - i - 1, j);
        }
    }
    return new_img;
}
